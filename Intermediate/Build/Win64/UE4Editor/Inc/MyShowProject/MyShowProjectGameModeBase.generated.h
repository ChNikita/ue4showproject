// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef MYSHOWPROJECT_MyShowProjectGameModeBase_generated_h
#error "MyShowProjectGameModeBase.generated.h already included, missing '#pragma once' in MyShowProjectGameModeBase.h"
#endif
#define MYSHOWPROJECT_MyShowProjectGameModeBase_generated_h

#define MyShowProject_Source_MyShowProject_MyShowProjectGameModeBase_h_15_RPC_WRAPPERS
#define MyShowProject_Source_MyShowProject_MyShowProjectGameModeBase_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define MyShowProject_Source_MyShowProject_MyShowProjectGameModeBase_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAMyShowProjectGameModeBase(); \
	friend struct Z_Construct_UClass_AMyShowProjectGameModeBase_Statics; \
public: \
	DECLARE_CLASS(AMyShowProjectGameModeBase, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MyShowProject"), NO_API) \
	DECLARE_SERIALIZER(AMyShowProjectGameModeBase)


#define MyShowProject_Source_MyShowProject_MyShowProjectGameModeBase_h_15_INCLASS \
private: \
	static void StaticRegisterNativesAMyShowProjectGameModeBase(); \
	friend struct Z_Construct_UClass_AMyShowProjectGameModeBase_Statics; \
public: \
	DECLARE_CLASS(AMyShowProjectGameModeBase, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MyShowProject"), NO_API) \
	DECLARE_SERIALIZER(AMyShowProjectGameModeBase)


#define MyShowProject_Source_MyShowProject_MyShowProjectGameModeBase_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AMyShowProjectGameModeBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AMyShowProjectGameModeBase) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AMyShowProjectGameModeBase); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AMyShowProjectGameModeBase); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AMyShowProjectGameModeBase(AMyShowProjectGameModeBase&&); \
	NO_API AMyShowProjectGameModeBase(const AMyShowProjectGameModeBase&); \
public:


#define MyShowProject_Source_MyShowProject_MyShowProjectGameModeBase_h_15_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AMyShowProjectGameModeBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AMyShowProjectGameModeBase(AMyShowProjectGameModeBase&&); \
	NO_API AMyShowProjectGameModeBase(const AMyShowProjectGameModeBase&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AMyShowProjectGameModeBase); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AMyShowProjectGameModeBase); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AMyShowProjectGameModeBase)


#define MyShowProject_Source_MyShowProject_MyShowProjectGameModeBase_h_15_PRIVATE_PROPERTY_OFFSET
#define MyShowProject_Source_MyShowProject_MyShowProjectGameModeBase_h_12_PROLOG
#define MyShowProject_Source_MyShowProject_MyShowProjectGameModeBase_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	MyShowProject_Source_MyShowProject_MyShowProjectGameModeBase_h_15_PRIVATE_PROPERTY_OFFSET \
	MyShowProject_Source_MyShowProject_MyShowProjectGameModeBase_h_15_RPC_WRAPPERS \
	MyShowProject_Source_MyShowProject_MyShowProjectGameModeBase_h_15_INCLASS \
	MyShowProject_Source_MyShowProject_MyShowProjectGameModeBase_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define MyShowProject_Source_MyShowProject_MyShowProjectGameModeBase_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	MyShowProject_Source_MyShowProject_MyShowProjectGameModeBase_h_15_PRIVATE_PROPERTY_OFFSET \
	MyShowProject_Source_MyShowProject_MyShowProjectGameModeBase_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	MyShowProject_Source_MyShowProject_MyShowProjectGameModeBase_h_15_INCLASS_NO_PURE_DECLS \
	MyShowProject_Source_MyShowProject_MyShowProjectGameModeBase_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MYSHOWPROJECT_API UClass* StaticClass<class AMyShowProjectGameModeBase>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID MyShowProject_Source_MyShowProject_MyShowProjectGameModeBase_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
